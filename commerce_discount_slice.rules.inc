<?php
/**
 * @file
 * Rules for commerce discount slice.
 */

/**
 * Implements hook_rules_action_info().
 */
function commerce_discount_slice_rules_action_info() {
  $items = array();

  $items['commerce_discount_slice_discount_action'] = array(
    'label' => t('Slice discount'),
    'group' => t('Commerce Discount'),
    'parameter' => array(
      'entity' => array(
        'label' => t('Entity'),
        'type' => 'entity',
        'wrapped' => TRUE,
      ),
      'commerce_discount' => array(
        'label' => t('Commerce Discount'),
        'type' => 'token',
        'options list' => 'commerce_discount_entity_list',
      ),
    ),
  );

  return $items;
}

/**
 * Action for discount slice.
 *
 * @param Object $order_wrapper
 *   Order wrapper object.
 * @param string $discount_name
 *   Name of the discount to apply.
 */
function commerce_discount_slice_discount_action($order_wrapper, $discount_name) {
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $discount_slice = $discount_wrapper->commerce_discount_offer->commerce_discount_slice;

  // Get total of products line items because we do not want to discount other
  // things like shipping.
  $line_items_total = commerce_line_items_total($order_wrapper->commerce_line_items, array('product'));

  $amount_discount = commerce_currency_convert($discount_slice->amount_discount->value(), $discount_slice->currency_code->value(), $line_items_total['currency_code']);
  $amount_slice = commerce_currency_convert($discount_slice->amount_slice->value(), $discount_slice->currency_code->value(), $line_items_total['currency_code']);
  $total_frequency_maximum = commerce_currency_convert($discount_slice->total_frequency_maximum->value(), $discount_slice->currency_code->value(), $line_items_total['currency_code']);
  $total_maximum = commerce_currency_convert($discount_slice->total_maximum->value(), $discount_slice->currency_code->value(), $line_items_total['currency_code']);
  $discount_quantity = floor($line_items_total['amount'] / $amount_slice);

  switch ($discount_slice->frequency->value()) {
    case 'order':
      $total_previous = $line_items_total['amount'];
      break;
    default:
      // Get all previous discounts of this name for this user based on
      // frequency.
      $total_previous = commerce_discount_slice_get_previous_discounts_total($discount_wrapper, $order_wrapper->uid->value());
      // Get total since the beginning of discount.
      $total_previous_all_time = commerce_discount_slice_get_previous_discounts_total($discount_wrapper, $order_wrapper->uid->value(), TRUE);
      break;
  }

  while (($total_previous_all_time + $discount_quantity * $amount_discount > $total_maximum && $total_maximum > 0)
    || ($total_previous + $discount_quantity * $amount_discount > $total_frequency_maximum && $total_frequency_maximum > 0)
    && $discount_quantity > 0) {
    $discount_quantity--;
  }

  if ($discount_quantity > 0) {
    $order = $order_wrapper->value();

    // Add discount to discounts informations in order.
    $delta = $order_wrapper->commerce_discounts->count();
    $order->commerce_discounts[LANGUAGE_NONE][$delta]['target_id'] = $discount_wrapper->discount_id->value();

    $discount_amount = $amount_discount;

    // Be sure we talk about positive number.
    if ($discount_amount > 0) {
      $discount_amount = -$discount_amount;
    }

    $discount_price = array(
      'amount' => $discount_amount,
      'currency_code' => $order_wrapper->commerce_order_total->currency_code->value(),
    );

    // Modify the existing discount line item or add a new one if that fails.
    if (!commerce_discount_slice_discount_set_existing_line_item_price($order_wrapper, $discount_name, $discount_price, $discount_quantity)) {
      commerce_discount_slice_discount_add_line_item($order_wrapper, $discount_name, $discount_price, $discount_quantity);
    }

    // Update the total order price, for the next rules condition (if any).
    commerce_order_calculate_total($order);
  }

}

/**
 * Get previous slice discounts total for a user.
 *
 * @param object $discount_wrapper
 *   Current discount wrapper.
 * @param int $uid
 *   User id.
 * @param bool $all_time
 *   Total depend of frequency or all time.
 *
 * @return int
 *   Total of current slice discount for this user.
 */
function commerce_discount_slice_get_previous_discounts_total($discount_wrapper, $uid, $all_time = FALSE) {
  $line_items = array();

  $discount_slice = $discount_wrapper->commerce_discount_offer->commerce_discount_slice;

  $created = new DateTime();
  $created->setTimestamp($discount_wrapper->created->value());
  if ($discount_slice->frequency->value() == 'no limit' || $all_time) {
    $current_date = $created;
  }
  else {
    $last_date = $current_date = $created;
    while ($last_date->getTimestamp() < time()) {
      $current_date = clone($last_date);
      $last_date = $last_date->add(
        DateInterval::createFromDateString($discount_slice->frequency->value())
      );
    }
  }

  $query = db_select('commerce_line_item', 'cli');
  $query->fields('cli', array('line_item_id', 'data'));
  $query->join('field_data_commerce_line_items', 'fdcli', 'cli.line_item_id = fdcli.commerce_line_items_line_item_id');
  $query->join('commerce_order', 'co', 'fdcli.entity_id = co.order_id');
  $query->condition('co.uid', $uid);
  $query->condition('cli.type', 'commerce_discount');
  $query->condition('co.created', $current_date->getTimestamp(), '>');
  $result = $query->execute();

  while ($line_item = $result->fetchAssoc()) {
    if (isset($line_item['data'])) {
      $data = unserialize($line_item['data']);
      if (isset($data['discount_name']) && $data['discount_name'] == $discount_wrapper->name->value()) {
        $line_items[] = $line_item['line_item_id'];
      }
    }
  }

  if (!empty($line_items)) {
    $line_items_total = commerce_line_items_total($line_items);
    // Discount total is negative, so return positive number.
    return -$line_items_total['amount'];
  }

  return 0;

}

/**
 * Add discount line item for commerce discount slice.
 *
 * Code from commerce_discount commerce_discount_add_line_item().
 *
 * @param \EntityDrupalWrapper $order_wrapper
 * Order wrapper.
 * @param string $discount_name
 * Discount name to apply.
 * @param int $discount_amount
 * Discount amount to apply.
 *
 * @return int
 */
function commerce_discount_slice_discount_add_line_item(EntityDrupalWrapper $order_wrapper, $discount_name, $discount_amount, $quantity = 1) {
  // Create a new line item.
  $values = array(
    'type' => 'commerce_discount',
    'order_id' => $order_wrapper->order_id->value(),
    'quantity' => $quantity,
    // Flag the line item.
    'data' => array('discount_name' => $discount_name),
  );

  $discount_line_item = entity_create('commerce_line_item', $values);
  $discount_line_item_wrapper = entity_metadata_wrapper('commerce_line_item', $discount_line_item);

  // Initialize the line item unit price.
  $discount_line_item_wrapper->commerce_unit_price->amount = 0;
  $discount_line_item_wrapper->commerce_unit_price->currency_code = $discount_amount['currency_code'];

  // Reset the data array of the line item total field to only include a
  // base price component, set the currency code from the order.
  $base_price = array(
    'amount' => 0,
    'currency_code' => $discount_amount['currency_code'],
    'data' => array(),
  );
  $discount_line_item_wrapper->commerce_unit_price->data = commerce_price_component_add($base_price, 'base_price', $base_price, TRUE);

  module_load_include('inc', 'commerce_discount', 'commerce_discount.rules');
  // Add the discount price component.
  commerce_discount_add_price_component($discount_line_item_wrapper, $discount_name, $discount_amount);

  // Save the line item and add it to the order.
  $discount_line_item_wrapper->save();
  $order_wrapper->commerce_line_items[] = $discount_line_item_wrapper;

  return $discount_line_item_wrapper->line_item_id->value();
}

/**
 * Updates the unit price of an existing lampiris discount line item.
 *
 * Non-discount line items are ignored.
 *
 * @param EntityDrupalWrapper $order_wrapper
 *   The wrapped order entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 * @param array $discount_price
 *   The discount amount price array (amount, currency_code).
 *
 * @return bool
 *   TRUE if an existing line item was successfully modified, FALSE otherwise.
 */
function commerce_discount_slice_discount_set_existing_line_item_price(EntityDrupalWrapper $order_wrapper, $discount_name, $discount_price, $quantity = 1) {
  $modified_existing = FALSE;
  module_load_include('inc', 'commerce_discount', 'commerce_discount.rules');
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {
    $line_item_data = $line_item_wrapper->value()->data;
    if ($line_item_wrapper->getBundle() == 'commerce_discount' && !empty($line_item_data['discount_name']) && $line_item_data['discount_name'] == $discount_name) {
      // Add the discount component price if the line item was originally
      // added by discount module.
      $line_item_wrapper->quantity = $quantity;
      commerce_discount_slice_discount_update_price_component($line_item_wrapper, $discount_name);
      $line_item_wrapper->save();
      $modified_existing = TRUE;
    }
  }

  return $modified_existing;
}

/**
 * Update a discount price component to the provided line item.
 *
 * @param EntityDrupalWrapper $line_item_wrapper
 *   The wrapped line item entity.
 * @param string $discount_name
 *   The name of the discount being applied.
 */
function commerce_discount_slice_discount_update_price_component(EntityDrupalWrapper $line_item_wrapper, $discount_name) {
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);
  $discount_wrapper = entity_metadata_wrapper('commerce_discount', $discount_name);
  $component_title = $discount_wrapper->component_title->value();
  $current_amount = $unit_price['amount'];

  // Update commerce_total so that the order total can be refreshed without
  // saving the line item. Taken from CommerceLineItemEntityController::save().
  $quantity = $line_item_wrapper->quantity->value();

  // Update the total of the line item based on the quantity and unit price.
  $unit_price = commerce_price_wrapper_value($line_item_wrapper, 'commerce_unit_price', TRUE);

  $line_item_wrapper->commerce_total->amount = $quantity * $unit_price['amount'];
  $line_item_wrapper->commerce_total->currency_code = $unit_price['currency_code'];

  // Add the components multiplied by the quantity to the data array.
  if (empty($unit_price['data']['components'])) {
    $unit_price['data']['components'] = array();
  }
  else {
    foreach ($unit_price['data']['components'] as $key => &$component) {
      $component['price']['amount'] *= $quantity;
    }
  }

  // Set the updated data array to the total price.
  $line_item_wrapper->commerce_total->data = $unit_price['data'];
}
